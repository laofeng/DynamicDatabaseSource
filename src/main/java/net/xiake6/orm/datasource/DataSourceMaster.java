package net.xiake6.orm.datasource;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * RUNTIME <br>
 * 编译器将把注释记录在类文件中，在运行时 VM 将保留注释，因此可以反射性地读取。<br>
 * ClassName:DataSourceMaster <br/>
 * Date: 2017年8月16日 上午9:05:04 <br/>
 * 
 * @author fenglibin
 * @version
 * @see
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DataSourceMaster {
}
