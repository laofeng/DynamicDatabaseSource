package net.xiake6.orm.datasource.sharding;

import java.util.Set;

import lombok.Data;
/**
 * 分库分表的配置
 * ClassName ShardingConfig.java
 * @author fenglibin
 * @Blog http://xiake6.net
 * @Date 2019年12月11日
 * 
 * Description
 */
@Data
public class ShardingConfig {
	/**
	 * 分表表名的set
	 */
	private Set<String> shardingTables;
	/**
	 * 数据库分库的ID选择的规则实现
	 */
	private ShardingCondition databaseShardingCondition;
	/**
	 * 数据表分表的ID选择的规则实现
	 */
	private ShardingCondition tableShardingCondition;

}
