package net.xiake6.orm.datasource.sharding;

import java.util.Map;

/**
 * 
 * ClassName TableShardingCondition.java
 * 
 * @author fenglibin
 * @Blog http://xiake6.net
 * @Date 2019年12月7日
 * 
 *       Description
 */
public interface ShardingCondition {
	/**
	 * 根据传入的Map参数值，返回用于数据库选择分库，或者用于数据表选择分表的shardingId。<br>
	 * 返回的值，是0～N-1的区间，N表示N个数据库分库，或者N表示N个数据表的分表。<br>
	 * 如有2个数据库，返回的值应当是在0~1这个区间，如有4个表，返回的值应该是0～3这个区间。<br>
	 * 程序会根据返回的值用于数据库分库的选择，或者数据表分表的选择。<br>
	 * 
	 * @param fieldNameAndValue 如果是数据库选择分库的应用场景，该参数代表的值为传入mybatisc
	 *                          mapper对应方法中的对象的字段名称及对应的值组成的map；<br>
	 *                          如果当前应用的是分表的应用场景，该参数代表的值为Where条件中字段名称与字段值的Map，这一点需要注意。
	 * @return 返回当前表对应的Sharding序号ID
	 */
	public int getShardingId(Map<String, Object> fieldNameAndValue);
}
