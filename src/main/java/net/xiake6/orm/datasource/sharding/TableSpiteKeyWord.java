package net.xiake6.orm.datasource.sharding;
/**
 * 不同的SQL语句中，表名前面的操作关键字
 * ClassName TableSpiteKeyWord.java
 * @author fenglibin
 * @Blog http://xiake6.net
 * @Date 2019年12月10日
 * 
 * Description
 */
public enum TableSpiteKeyWord {
	FROM, //表示Select和Delete操作中，表名是出现在FROM关键字后
	INTO, //表示Insert操作中，表名是出现在INTO关键字后
	UPDATE; //表示Update操作中，表名是出现在UPDATE关键字后
}
