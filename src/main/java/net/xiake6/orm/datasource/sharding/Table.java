package net.xiake6.orm.datasource.sharding;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
@Data
public class Table {
	private String originalSql;
	private String nowSql;
	private Map<String,String> oldAndNewTable = new HashMap<String,String>();
}
