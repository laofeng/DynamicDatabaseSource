package net.xiake6.orm.datasource.sharding;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import net.xiake6.orm.datasource.HandleDataSource;
/**
 * 自定义的数据源，通过ThreadLocal来传递当前选择的数据源
 * ClassName DynamicShardingDataSource.java
 * @author fenglibin
 * @Blog http://xiake6.net
 * @Date 2019年12月11日
 * 
 * Description
 */
public class DynamicShardingDataSource extends AbstractRoutingDataSource {

	@Override
	protected Object determineCurrentLookupKey() {
		Object obj = HandleDataSource.getDataSource();
		// logger.info("current lookup key is:" + obj);
		return obj;
	}
}
