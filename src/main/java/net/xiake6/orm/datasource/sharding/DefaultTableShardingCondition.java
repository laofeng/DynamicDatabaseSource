package net.xiake6.orm.datasource.sharding;

import java.util.Map;
import java.util.Set;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 分表的默认实现
 * ClassName DefaultTableShardingCondition.java
 * @author fenglibin
 * @Blog http://xiake6.net
 * @Date 2019年12月11日
 * 
 * Description
 */
@Slf4j
public class DefaultTableShardingCondition implements ShardingCondition {

	@Setter
	private int tableNums;

	@Override
	public int getShardingId(Map<String, Object> fieldNameAndValue) {
		log.debug("使用传入字段中的第一个字段做为Sharding字段的示例");
		if (tableNums > 0) {
			Set<Map.Entry<String, Object>> set = fieldNameAndValue.entrySet();
			for (Map.Entry<String, Object> entry : set) {
				return (tableNums-1) & entry.getValue().hashCode();
			}
		}
		return -1;
	}

}
