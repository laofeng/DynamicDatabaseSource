package net.xiake6.orm.datasource;

/**
 * ClassName:DataSourceType <br/>
 * Date: 2017年8月16日 下午1:41:46 <br/>
 * 
 * @author fenglibin
 * @version
 * @see
 */
public enum DataSourceType {
                            MASTER("master"), SLAVE("slave");

    private String value;

    private DataSourceType(String value){
        this.value = value;
    }

    public String toString() {
        return value;
    }
}
