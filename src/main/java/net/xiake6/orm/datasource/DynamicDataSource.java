package net.xiake6.orm.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * ClassName:DynamicDataSource <br/>
 * Date: 2017年8月16日 上午9:07:06 <br/>
 * 
 * @author fenglibin
 * @version
 * @see
 */

public class DynamicDataSource extends AbstractRoutingDataSource {

    /**
     * 获取与数据源相关的key 此key是Map<String,DataSource> resolvedDataSources 中与数据源绑定的key值 在通过determineTargetDataSource获取目标数据源时使用
     */
    @Override
    protected Object determineCurrentLookupKey() {
        Object obj = HandleDataSource.getDataSource();
        //logger.info("current lookup key is:" + obj);
        return obj;
    }

}
