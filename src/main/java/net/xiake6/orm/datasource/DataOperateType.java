package net.xiake6.orm.datasource;

/**
 * 对数据库中数据的操作类型 ClassName:OperateType <br/>
 * Date: 2017年8月16日 下午2:23:02 <br/>
 * 
 * @author fenglibin
 * @version
 * @see
 */
public enum DataOperateType {

                             INSERT("insert"), UPDATE("update"), DELETE("delete"), SELECT("select"), GET("get"),
                             QUERY("query");

    private String name;

    private DataOperateType(String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}
