package net.xiake6.orm.datasource;

import lombok.Setter;

/**
 * 保存当前线程数据源的key <br>
 * ClassName:HandleDataSource <br/>
 * Date: 2017年8月16日 上午9:08:20 <br/>
 * 
 * @author fenglibin
 * @version
 * @see
 */
public class HandleDataSource {

	public static final ThreadLocal<String> holder = new ThreadLocal<String>();

	/**
	 * 绑定当前线程数据源路由的key
	 * 
	 * @param key
	 */
	public static void putDataSource(String datasource) {
		holder.set(datasource);
	}

	/**
	 * 获取当前线程的数据源路由的key
	 * 
	 * @return
	 */
	public static String getDataSource() {
		return holder.get();
	}
}
