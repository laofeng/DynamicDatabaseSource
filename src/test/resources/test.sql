CREATE TABLE `apps` (
  `package_name` varchar(128) NOT NULL COMMENT 'App的包名',
  `name` varchar(128) NOT NULL COMMENT 'App的名称',
  `icon` varchar(128) DEFAULT NULL COMMENT 'App的ICON地址',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `country_code` varchar(128) DEFAULT NULL COMMENT '国家代码',
  `headerquarters` varchar(128) DEFAULT NULL COMMENT '总司总部所在国家代码',
  `company_name` varchar(128) DEFAULT NULL COMMENT '公司名称',
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '加入的日期',
  `views` int(11) DEFAULT NULL COMMENT '跳转到google play的次数',
  PRIMARY KEY (`package_name`)
);
CREATE TABLE `apps_0` (
  `package_name` varchar(128) NOT NULL COMMENT 'App的包名',
  `name` varchar(128) NOT NULL COMMENT 'App的名称',
  `icon` varchar(128) DEFAULT NULL COMMENT 'App的ICON地址',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `country_code` varchar(128) DEFAULT NULL COMMENT '国家代码',
  `headerquarters` varchar(128) DEFAULT NULL COMMENT '总司总部所在国家代码',
  `company_name` varchar(128) DEFAULT NULL COMMENT '公司名称',
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '加入的日期',
  `views` int(11) DEFAULT NULL COMMENT '跳转到google play的次数',
  PRIMARY KEY (`package_name`)
);
CREATE TABLE `apps_1` (
  `package_name` varchar(128) NOT NULL COMMENT 'App的包名',
  `name` varchar(128) NOT NULL COMMENT 'App的名称',
  `icon` varchar(128) DEFAULT NULL COMMENT 'App的ICON地址',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `country_code` varchar(128) DEFAULT NULL COMMENT '国家代码',
  `headerquarters` varchar(128) DEFAULT NULL COMMENT '总司总部所在国家代码',
  `company_name` varchar(128) DEFAULT NULL COMMENT '公司名称',
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '加入的日期',
  `views` int(11) DEFAULT NULL COMMENT '跳转到google play的次数',
  PRIMARY KEY (`package_name`)
);
CREATE TABLE `apps_2` (
  `package_name` varchar(128) NOT NULL COMMENT 'App的包名',
  `name` varchar(128) NOT NULL COMMENT 'App的名称',
  `icon` varchar(128) DEFAULT NULL COMMENT 'App的ICON地址',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `country_code` varchar(128) DEFAULT NULL COMMENT '国家代码',
  `headerquarters` varchar(128) DEFAULT NULL COMMENT '总司总部所在国家代码',
  `company_name` varchar(128) DEFAULT NULL COMMENT '公司名称',
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '加入的日期',
  `views` int(11) DEFAULT NULL COMMENT '跳转到google play的次数',
  PRIMARY KEY (`package_name`)
);
CREATE TABLE `apps_3` (
  `package_name` varchar(128) NOT NULL COMMENT 'App的包名',
  `name` varchar(128) NOT NULL COMMENT 'App的名称',
  `icon` varchar(128) DEFAULT NULL COMMENT 'App的ICON地址',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `country_code` varchar(128) DEFAULT NULL COMMENT '国家代码',
  `headerquarters` varchar(128) DEFAULT NULL COMMENT '总司总部所在国家代码',
  `company_name` varchar(128) DEFAULT NULL COMMENT '公司名称',
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '加入的日期',
  `views` int(11) DEFAULT NULL COMMENT '跳转到google play的次数',
  PRIMARY KEY (`package_name`)
);
CREATE TABLE `apps_4` (
  `package_name` varchar(128) NOT NULL COMMENT 'App的包名',
  `name` varchar(128) NOT NULL COMMENT 'App的名称',
  `icon` varchar(128) DEFAULT NULL COMMENT 'App的ICON地址',
  `seq` int(11) DEFAULT NULL COMMENT '排序',
  `country_code` varchar(128) DEFAULT NULL COMMENT '国家代码',
  `headerquarters` varchar(128) DEFAULT NULL COMMENT '总司总部所在国家代码',
  `company_name` varchar(128) DEFAULT NULL COMMENT '公司名称',
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '加入的日期',
  `views` int(11) DEFAULT NULL COMMENT '跳转到google play的次数',
  PRIMARY KEY (`package_name`)
);