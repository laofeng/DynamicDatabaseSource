package net.xiake6.orm.persistence.bean;

import java.io.Serializable;
import java.util.Date;

public class Apps implements Serializable{

    private static final long serialVersionUID = 3238352531424378609L;

    private String  packageName="packageName";

    private String  name="name";

    private String  icon="icon";

    private Integer seq=1;

    private String  countryCode="countryCode";

    private String  headerquarters="headerquarters";

    private String  companyName="companyName";

    private Date    added = new Date();

    private Integer views=1;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getHeaderquarters() {
        return headerquarters;
    }

    public void setHeaderquarters(String headerquarters) {
        this.headerquarters = headerquarters;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Date getAdded() {
        return added;
    }

    public void setAdded(Date added) {
        this.added = added;
    }
    
    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }
}
