package net.xiake6.orm.persistence.mapper;

import java.util.List;
import java.util.Map;

import net.xiake6.orm.persistence.bean.Apps;

public interface AppsMapper {
	List<Apps> select(Apps record);
	
	List<Apps> select2(Apps record);
	
	List<Apps> select3(String str);
	
	List<Apps> select4(int i);
	
	List<Apps> select5(Map<String,Object> map);
	
	List<Apps> select6(List<Integer> seqs);
	
	List<Apps> select7(int i,String str);

	int delete(Apps record);

	int insert(Apps record);

	int update(Apps record);
}
