package net.xiake6.orm.datasource;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;
import net.xiake6.orm.datasource.sharding.TableParser;

@Slf4j
public class TableParserTest {
	@Test
	public void test1() {
		String sql = "select * from apps where 1=11 and 2=22 and 3=33 \n"
				+ "    and 4=44 and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		List<String> tableList = TableParser.getTableListFromSql(sql);
		tableList.forEach(t -> {
			log.info(t);
		});
		Assert.assertEquals(1, tableList.size());
		Assert.assertEquals("apps",tableList.get(0));
	}

	@Test
	public void test2() {
		String sql = "select * from apps where 1=11 and 2=22 and 3=33 \n"
				+ "    and 4=(select views from other_table where seq=#{seq,jdbcType=INTEGER})\n"
				+ "    and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		List<String> tableList = TableParser.getTableListFromSql(sql);
		tableList.forEach(t -> {
			log.info(t);
		});
		Assert.assertEquals(2, tableList.size());
		Assert.assertEquals("apps",tableList.get(0));
		Assert.assertEquals("other_table",tableList.get(1));
	}

	@Test
	public void test2_1() {
		String sql = "select from_field from apps where 1=11 and 2=22 and 3=33 \n"
				+ "    and 4=(select views from other_table where seq=#{seq,jdbcType=INTEGER})\n"
				+ "    and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		List<String> tableList = TableParser.getTableListFromSql(sql);
		tableList.forEach(t -> {
			log.info(t);
		});
		Assert.assertEquals(2, tableList.size());
		Assert.assertEquals("apps",tableList.get(0));
		Assert.assertEquals("other_table",tableList.get(1));
	}

	@Test
	public void test2_2() {
		String sql = "select field_from from apps where 1=11 and 2=22 and 3=33 \n"
				+ "    and 4=(select views from other_table where seq=#{seq,jdbcType=INTEGER})\n"
				+ "    and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		List<String> tableList = TableParser.getTableListFromSql(sql);
		tableList.forEach(t -> {
			log.info(t);
		});
		Assert.assertEquals(2, tableList.size());
		Assert.assertEquals("apps",tableList.get(0));
		Assert.assertEquals("other_table",tableList.get(1));
	}

	@Test
	public void test2_3() {
		String sql = "select from_field,field_from from apps where 1=11 and 2=22 \n"
				+ "    and 3=33 and 4=(select views from other_table where seq=#{seq,jdbcType=INTEGER})\n"
				+ "    and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		List<String> tableList = TableParser.getTableListFromSql(sql);
		tableList.forEach(t -> {
			log.info(t);
		});
		Assert.assertEquals(2, tableList.size());
		Assert.assertEquals("apps",tableList.get(0));
		Assert.assertEquals("other_table",tableList.get(1));
	}

	@Test
	public void test2_4() {
		String sql = "select from_field,field_from from apps where 1=11 and field_from=22 \n"
				+ "    and from_field=33 \n"
				+ "    and 4=(select views from other_table where seq=#{seq,jdbcType=INTEGER})\n"
				+ "    and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		List<String> tableList = TableParser.getTableListFromSql(sql);
		tableList.forEach(t -> {
			log.info(t);
		});
		Assert.assertEquals(2, tableList.size());
		Assert.assertEquals("apps",tableList.get(0));
		Assert.assertEquals("other_table",tableList.get(1));
	}

	@Test
	public void test2_5() {
		String sql = "select from_field,field_from from (select from_field,field_from,a,b,c from apps where a=1 and b=2) \n"
				+ "    where 1=11 and field_from=22 and from_field=33 \n"
				+ "    and 4=(select views from other_table where seq=#{seq,jdbcType=INTEGER})\n"
				+ "    and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		List<String> tableList = TableParser.getTableListFromSql(sql);
		tableList.forEach(t -> {
			log.info(t);
		});
		Assert.assertEquals(2, tableList.size());
		Assert.assertEquals("apps",tableList.get(0));
		Assert.assertEquals("other_table",tableList.get(1));
	}

	@Test
	public void test3() {
		String sql = "select * from apps where 1=11 and 2=22 and 3=33 \n"
				+ "    and 4=44 and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		Set<String> shardingTableSet = new HashSet<String>();
		shardingTableSet.add("apps");
		int tableShadingId = 1;
		sql = TableParser.getShardingTableSql(sql, shardingTableSet, tableShadingId);
		log.info(sql);
		Assert.assertTrue(sql.indexOf("apps_1")>0);
	}

	/**
	 * 测试条件字段中包含了与表名相关的字段的情况
	 */
	@Test
	public void test4() {
		String sql = "select * from apps where 1=11 and 2=(select id from appsok where a=b) \n"
				+ "    and 3=33 and apps=44 and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		Set<String> shardingTableSet = new HashSet<String>();
		shardingTableSet.add("apps");
		int tableShadingId = 1;
		sql = TableParser.getShardingTableSql(sql, shardingTableSet, tableShadingId);
		log.info(sql);
		Assert.assertTrue(sql.indexOf("apps_1")>0);
		Assert.assertFalse(sql.indexOf("appsok_1")>0);
	}

	/**
	 * 测试字段中包含了from关键字的情况
	 */
	@Test
	public void test5() {
		String sql = "select from_field from apps where 1=11 \n"
				+ "    and 2=(select id from appsok where a=b) and 3=33 and apps=44 \n"
				+ "    and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		Set<String> shardingTableSet = new HashSet<String>();
		shardingTableSet.add("apps");
		int tableShadingId = 1;
		sql = TableParser.getShardingTableSql(sql, shardingTableSet, tableShadingId);
		log.info(sql);
		Assert.assertTrue(sql.indexOf("apps_1")>0);
		Assert.assertFalse(sql.indexOf("appsok_1")>0);
		Assert.assertFalse(sql.indexOf("from_1")>0);
	}

	/**
	 * 测试字段中包含了from关键字的情况
	 */
	@Test
	public void test5_1() {
		String sql = "select field_from from apps where 1=11 \n"
				+ "    and 2=(select id from appsok where a=b) and 3=33 and apps=44 \n"
				+ "    and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		Set<String> shardingTableSet = new HashSet<String>();
		shardingTableSet.add("apps");
		int tableShadingId = 1;
		sql = TableParser.getShardingTableSql(sql, shardingTableSet, tableShadingId);
		log.info(sql);
		Assert.assertTrue(sql.indexOf("apps_1")>0);
		Assert.assertFalse(sql.indexOf("appsok_1")>0);
		Assert.assertFalse(sql.indexOf("from_1")>0);
	}

	/**
	 * 测试表名后直接带换行符的情况
	 */
	@Test
	public void test6() {
		String sql = "select * from apps\nwhere 1=11 and 2=22 and 3=33 \n"
				+ "    and 4=44 and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		Set<String> shardingTableSet = new HashSet<String>();
		shardingTableSet.add("apps");
		int tableShadingId = 1;
		sql = TableParser.getShardingTableSql(sql, shardingTableSet, tableShadingId);
		log.info(sql);
		Assert.assertTrue(sql.indexOf("apps_1")>0);
		
	}

	@Test
	public void test7() {
		String sql = "select from_field,field_from from apps where 1=11 and field_from=22 \n"
				+ "    and from_field=33 \n"
				+ "    and 4=(select views from other_table where seq=#{seq,jdbcType=INTEGER})\n"
				+ "    and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		Set<String> shardingTableSet = new HashSet<String>();
		shardingTableSet.add("apps");
		int tableShadingId = 1;
		sql = TableParser.getShardingTableSql(sql, shardingTableSet, tableShadingId);
		log.info(sql);
		Assert.assertTrue(sql.indexOf("apps_1")>0);
		Assert.assertFalse(sql.indexOf("appsok_1")>0);
		Assert.assertFalse(sql.indexOf("from_1")>0);
		Assert.assertFalse(sql.indexOf("other_table_1")>0);
	}

	@Test
	public void test7_1() {
		String sql = "select from_field,field_from from (select 1 from apps where a=b) where 1=11 \n"
				+ "    and field_from=22 and from_field=33 \n"
				+ "    and 4=(select views from other_table where seq=#{seq,jdbcType=INTEGER})\n"
				+ "    and package_name = #{packageName,jdbcType=VARCHAR}\n"
				+ "    and name = #{name,jdbcType=VARCHAR}and seq = #{seq,jdbcType=INTEGER}\n"
				+ "    limit 10";
		Set<String> shardingTableSet = new HashSet<String>();
		shardingTableSet.add("apps");
		int tableShadingId = 1;
		sql = TableParser.getShardingTableSql(sql, shardingTableSet, tableShadingId);
		log.info(sql);
		Assert.assertTrue(sql.indexOf("apps_1")>0);
		Assert.assertFalse(sql.indexOf("appsok_1")>0);
		Assert.assertFalse(sql.indexOf("from_1")>0);
		Assert.assertFalse(sql.indexOf("other_table_1")>0);
	}

	@Test
	public void test8() {
		String sql = "insert into apps(a,b,c) values(1,2,3);";
		List<String> tableList = TableParser.getTableListFromSql(sql);
		tableList.forEach(t -> {
			log.info(t);
		});
		Assert.assertEquals(1, tableList.size());
		Assert.assertEquals("apps",tableList.get(0));
	}

	@Test
	public void test9() {
		String sql = "insert into apps(a,b,c) values(1,2,3);";
		Set<String> shardingTableSet = new HashSet<String>();
		shardingTableSet.add("apps");
		int tableShadingId = 1;
		sql = TableParser.getShardingTableSql(sql, shardingTableSet, tableShadingId);
		log.info(sql);
		Assert.assertTrue(sql.indexOf("apps_1")>0);
	}
	@Test
	public void test10() {
		String sql = "insert into apps(a,b,c) values(1,2,3);insert into apps0(a,b,c) values(4,5,6);";
		Set<String> shardingTableSet = new HashSet<String>();
		shardingTableSet.add("apps");
		int tableShadingId = 1;
		sql = TableParser.getShardingTableSql(sql, shardingTableSet, tableShadingId);
		log.info(sql);
		Assert.assertTrue(sql.indexOf("apps_1")>0);
		Assert.assertFalse(sql.indexOf("apps0_1")>0);
	}
	@Test
	public void test11() {
		String sql = "insert into apps select * from apps1 where a=b";
		Set<String> shardingTableSet = new HashSet<String>();
		shardingTableSet.add("apps");
		int tableShadingId = 1;
		sql = TableParser.getShardingTableSql(sql, shardingTableSet, tableShadingId);
		log.info(sql);
		Assert.assertTrue(sql.indexOf("apps_1")>0);
		Assert.assertFalse(sql.indexOf("apps1_1")>0);
	}
}
