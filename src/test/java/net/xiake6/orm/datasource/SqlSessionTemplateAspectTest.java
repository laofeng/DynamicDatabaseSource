package net.xiake6.orm.datasource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;
import net.xiake6.orm.persistence.bean.Apps;
import net.xiake6.orm.persistence.mapper.AppsMapper;

@Slf4j
public class SqlSessionTemplateAspectTest extends BaseTestCase {
	@Resource
	private AppsMapper appsMapper;

	@Test
	public void test1() throws InterruptedException {
		for (int i = 0; i < 100; i++) {
			log(5 & System.currentTimeMillis());
			Thread.sleep(100);
		}
	}

	@Test
	public void testSelect() throws InterruptedException {
		Apps apps = new Apps();
		List<Apps> str = appsMapper.select(apps);
		log(str.size());
	}

	@Test
	public void testSelect2() throws InterruptedException {
		Apps apps = new Apps();
		List<Apps> str = appsMapper.select2(apps);
		log(str.size());
	}
	
	@Test
	public void testSelect3() throws InterruptedException {
		List<Apps> str = appsMapper.select3("apps");
		log(str.size());
	}
	
	@Test
	public void testSelect4() throws InterruptedException {
		List<Apps> str = appsMapper.select4(123);
		log(str.size());
	}
	
	@Test
	public void testSelect5() throws InterruptedException {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("packageName", "con.test.abc");
		map.put("seq", 123);
		List<Apps> str = appsMapper.select5(map);
		log(str.size());
	}
	
	@Test
	public void testSelect6() throws InterruptedException {
		List<Integer> seqs = new ArrayList<Integer>();
		seqs.add(123);
		seqs.add(456);
		List<Apps> str = appsMapper.select6(seqs);
		log(str.size());
	}
	
	@Test
	public void testSelect7() throws InterruptedException {
		List<Apps> str = appsMapper.select7(123,"apps");
		log(str.size());
	}

	@Test
	public void testUpdate() throws InterruptedException {
		Apps apps = new Apps();
		int result = appsMapper.update(apps);
		log(result);
	}

	@Test
	public void testDelete() throws InterruptedException {
		Apps apps = new Apps();
		int result = appsMapper.delete(apps);
		log(result);
	}

	@Test
	public void testInsert() throws InterruptedException {
		Apps apps = new Apps();
		int result = appsMapper.insert(apps);
		log(result);
	}

	@Test
	public void testInsert2() throws InterruptedException {
		Apps apps = new Apps();
		apps.setCompanyName(null);
		int result = appsMapper.insert(apps);
		log(result);
	}

	private void log(Object obj) {
		log.info(String.valueOf(obj));
	}
}
